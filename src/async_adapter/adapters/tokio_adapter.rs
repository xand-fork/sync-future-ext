#[cfg(test)]
mod tests;

use super::super::error::Error;
use super::super::ports::future_resolver::FutureResolver;

use lazy_static::lazy_static;
use tokio::{
    macros::support::Future,
    runtime::{Handle, Runtime},
};

/// Adapter for managing and accessing a tokio runtime
pub struct TokioAdapter {
    pub(super) inner: Handle,
}

impl TokioAdapter {
    pub fn get() -> Result<Self, Error> {
        let handle = match TOKIO_RUNTIME.as_ref() {
            Ok(rt) => rt.handle().clone(),
            Err(err) => return Err(err.clone()),
        };

        Ok(Self { inner: handle })
    }
}

impl FutureResolver for TokioAdapter {
    fn block<F: Future>(&self, fut: F) -> <F as Future>::Output {
        self.inner.block_on(fut)
    }
}

lazy_static! {
    /// This is the shared instance of a Tokio Runtime, only to be accessed via
    /// the `TokioRuntime` struct above
    static ref TOKIO_RUNTIME: Result<Runtime, Error> = {
        Runtime::new().map_err(|err| Error::TokioRuntimeInitializiation (err.to_string()))
    };
}
