#![allow(non_snake_case)]
// Clippy is a bit bugged on this so disabling the lint for now
// https://gitlab.com/TransparentIncDevelopment/product/libs/sync-future-ext/-/jobs/2625552731
#![allow(clippy::semicolon_if_nothing_returned)]
use super::*;

/// Test in a default, sync context by not using the `#[tokio::test]` annotation
#[test]
fn get__returns_ok_in_synchronous_context() {
    // Then
    let _t = TokioAdapter::get().unwrap();
}

/// Test invoking within a tokio runtime by using the #[tokio::test] annotation
#[tokio::test]
async fn get__returns_ok_in_asynchronous_runtime() {
    // Then
    let _t = TokioAdapter::get().unwrap();
}

#[test]
fn block__can_resolve_future() {
    // Given
    #[allow(clippy::unused_async)]
    async fn async_fn() -> u32 {
        123
    }
    let t = TokioAdapter::get().unwrap();

    // When
    let res = t.block(async_fn());

    // Then
    assert_eq!(res, 123);
}
