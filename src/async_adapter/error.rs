use thiserror::Error;

#[derive(Clone, Debug, Error)]
pub enum Error {
    #[error("fix this")]
    TokioRuntimeInitializiation(String),
}
